package main.scala

import scala.collection.immutable.List

/**
  * Created by Sheng on 11/19/16.
  */
// Below are the main data models that are used in this application.
case class Question(qid: Long, question: String, answer: List[String])
case class AnsweredQuestion(qid: Long, answer: String)
case class JobApplication(aid: String, answeredQuestions: List[AnsweredQuestion])


