import com.google.inject.{AbstractModule, Guice}
import com.google.inject.name.Names
import main.scala._
import net.codingwell.scalaguice.ScalaModule
import org.scalatest._

class ApplicationProcessorSpec extends FlatSpec with Matchers {

  val injector = Guice.createInjector(new ApplicationProcessorTestModule())
  import net.codingwell.scalaguice.InjectorExtensions._

  val jqp = injector.instance[JsonQuestionProvider]
  val ap = injector.instance[ApplicationProcessor]

  "Empty application list" should "return empty list application" in {
    val applications = JsonApplicationProvider(
      """ [ ]"""
    ).getApplications

    val result = ap.acceptList(applications)
    result.get.size should be (0)
  }

  "List of one valid application" should "return one valid application" in {
    val applications = JsonApplicationProvider(
      """ [ { "aid" : "ABC", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazy" } ] } ]"""
    ).getApplications

    val result = ap.acceptList(applications)
    result.get.size should be (1)
    result.get.head.aid should be ("ABC")
  }

  "List of one invalid application" should "return zero application" in {
    val applications = JsonApplicationProvider(
      """ [ { "aid" : "ABC", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazzy" } ] } ]"""
    ).getApplications

    val result = ap.acceptList(applications)
    result.get.size should be (0)
  }

  "List of all valid applications" should "return all applications" in {
    val applications = JsonApplicationProvider(
      """ [
        | { "aid" : "ABC1", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazy" } ] },
        | { "aid" : "ABC2", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazy" } ] }
        | { "aid" : "ABC3", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazy" } ] }
        | { "aid" : "ABC4", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazy" } ] }
        |]""".stripMargin
    ).getApplications

    val result = ap.acceptList(applications)
    result.get.size should be (4)
  }

  "List of all invalid applications" should "return zero application" in {
    val applications = JsonApplicationProvider(
      """ [
        | { "aid" : "ABC1", "answeredQuestions": [{ "qid" : 1, "answer": "world1" }, { "qid" : 2, "answer": "lazy" } ] },
        | { "aid" : "ABC2", "answeredQuestions": [{ "qid" : 1, "answer": "world1" }, { "qid" : 2, "answer": "lazy" } ] }
        | { "aid" : "ABC3", "answeredQuestions": [{ "qid" : 1, "answer": "world1" }, { "qid" : 2, "answer": "lazy" } ] }
        | { "aid" : "ABC4", "answeredQuestions": [{ "qid" : 1, "answer": "world1" }, { "qid" : 2, "answer": "lazy" } ] }
        |]""".stripMargin
    ).getApplications

    val result = ap.acceptList(applications)
    result.get.size should be (0)
  }

  "List of some invalid appliations and one valid application" should "return one application" in {
    val applications = JsonApplicationProvider(
      """ [
        | { "aid" : "ABC1", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazzy" } ] },
        | { "aid" : "ABC2", "answeredQuestions": [{ "qid" : 1, "answer": "world1" }, { "qid" : 2, "answer": "lazzy" } ] }
        | { "aid" : "ABC3", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazy" } ] }
        | { "aid" : "ABC4", "answeredQuestions": [{ "qid" : 1, "answer": "1world" }, { "qid" : 2, "answer": "lazy" } ] }
        |]""".stripMargin
    ).getApplications

    val result = ap.acceptList(applications)
    result.get.size should be (1)
  }

  "Error application input " should "return a Failure" in {
    val applications = JsonApplicationProvider(
      """ [
        | { "aid" : 1"ABC1", "answeredQuestions": [{ "qid" : 1, "answer": "world" }, { "qid" : 2, "answer": "lazzy" } ] }
        |]""".stripMargin
    ).getApplications
    val result = ap.acceptList(applications)
    result.getClass.getSimpleName should be ("Failure")
  }
}

class ApplicationProcessorTestModule extends AbstractModule with ScalaModule{
  def configure(): Unit = {
    bind[ApplicationProcessor].to[ApplicationProcessorImpl]
    bind[QuestionProvider].to[JsonQuestionProvider]
    // One example of instance binding
    bind[String].annotatedWith(Names.named("Question JSON String"))
      .toInstance(
        """ [
          |{ "qid" : 1, "question": "hello", "answer": ["world"] },
          |{ "qid" : 2, "question": "The quick brown fox", "answer": ["jumps", "over", "the", "lazy", "dog"] },
          |{ "qid" : 3, "question": "foo", "answer": ["bar"] },
          |{ "qid" : 4, "question": "Do you have a car?", "answer": ["Yes"] }
          |] """.stripMargin
      )
  }
}