package main.scala

import com.google.inject.Inject
import com.google.inject.name.{Named, Names}
import org.json4s.native.Serialization._
import org.json4s.DefaultFormats

import scala.collection.immutable.List
import scala.collection.mutable

/**
  * Created by Sheng on 11/19/16.
  */
trait QuestionProvider {
  implicit val formats = DefaultFormats
  def inputString: String
  lazy val getQuestions = read[List[Question]](inputString)

  private val _rightAnswersDict: mutable.HashMap[Long, List[String]] = mutable.HashMap()
  lazy val rightAnswersDict = {
    getQuestions.foreach(q => _rightAnswersDict.put(q.qid, q.answer))
    _rightAnswersDict
  }
}

class JsonQuestionProvider @Inject() (@Named("Question JSON String") _inputString: String) extends QuestionProvider {
  def inputString = _inputString
}

class JsonFileQuestionProvider @Inject() (@Named("Question File Path") filePath: String) extends QuestionProvider {
  def inputString = {
    scala.io.Source.fromFile(s"data/${filePath}").mkString
  }
}