package main.scala

import com.google.inject.Inject

import scala.collection.immutable.List
import scala.util.{Success, Failure, Try}

trait ApplicationProcessor {
  def acceptList(applications: Try[List[JobApplication]]): Try[List[JobApplication]]
  def acceptOne(application: JobApplication): Option[JobApplication]
}

/**
 * All the classes are defined here to make it easier to review.
 * Normal structure will have classes well organized.
 */
class ApplicationProcessorImpl @Inject()
  (rightAnswers: QuestionProvider) extends ApplicationProcessor{
  /**
   * This is the main method we need to test.
   * @param applications
   * @return
   */
  def acceptList(applications: Try[List[JobApplication]]): Try[List[JobApplication]] = {
    applications match {
      case Success(v) => Try(v.flatMap(acceptOne))
      case Failure(e) => Failure(ApplicationProcessingError("Application input parsing failed, please verify whether it has valid JSON structure!"))
    }
  }

  def acceptOne(application: JobApplication): Option[JobApplication] = {
    val answeredQuestions = application.answeredQuestions
    // Function exists here will guarantee early termination.
    // Using Option is one way to avoid using IF ELSE and return an Option here.
    Option(answeredQuestions.exists(
      question => {
        !rightAnswers.rightAnswersDict(question.qid).contains(question.answer)
      }
    )).collect{
      // No unaccepted answers means this is a valid application
      case false => application
    }
  }
}

case class ApplicationProcessingError(errorMessage: String) extends Exception
