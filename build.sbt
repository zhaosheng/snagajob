name := "ApplicationProcessor"

version := "1.0"

scalaVersion := "2.11.8"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

val json4sNative = "org.json4s" %% "json4s-native" % "3.5.0"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.0"
val scalaGuice = "net.codingwell" %% "scala-guice" % "4.1.0"

libraryDependencies ++= Seq(
  json4sNative,
  scalaTest,
  scalaGuice
)

scalaSource in Compile := baseDirectory.value / "src"

// lazy val accept = taskKey[Int]("Process applications and return all accepted ones.")

lazy val ApplicationProcesser = (project in file(".")).
  settings(
    name := "ApplicationProcessor",
    version := "0.1",
    scalaVersion := "2.11.8"
  )

mainClass in (Compile, run) := Some("main.scala.JobApplicationProcessingApp")