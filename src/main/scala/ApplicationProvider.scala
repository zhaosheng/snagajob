package main.scala

import org.json4s.native.Serialization._
import org.json4s.DefaultFormats

import scala.util.Try

/**
  * Created by Sheng on 11/19/16.
  */
trait ApplicationProvider {
  implicit val formats = DefaultFormats
  def inputString: String
  def getApplications: Try[List[JobApplication]] = Try {
    read[List[JobApplication]](inputString)
  }
}

class JsonApplicationProvider(_inputString: String) extends ApplicationProvider {
  def inputString = _inputString
}

object JsonApplicationProvider {
  /**
    * This is an example of using companion object to create case class like constructor.
    * @param input String content of the JSON
    * @return
    */
  def apply(input: String) = new JsonApplicationProvider(input)
}

class JsonFileApplicationProvider(_inputString: String) extends ApplicationProvider{
  def inputString = _inputString
}

object JsonFileApplicationProvider {
  def apply(fileLocation: String) = {
    val lines = scala.io.Source.fromFile(s"data/${fileLocation}").mkString
    new JsonFileApplicationProvider(lines)
  }
}