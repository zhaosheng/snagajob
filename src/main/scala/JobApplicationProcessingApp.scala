package main.scala

import java.io.FileWriter

import com.google.inject.Guice
import org.json4s.DefaultFormats
import org.json4s.native.Serialization._

import scala.util.{Failure, Success}

/**
  * Created by Sheng on 11/19/16.
  */
object JobApplicationProcessingApp extends App {
  // For writing JSON to string
  implicit val formats = DefaultFormats

  // https://github.com/codingwell/scala-guice
  val injector = Guice.createInjector(new ApplicationProcessorModule())
  import net.codingwell.scalaguice.InjectorExtensions._

  val jfqp = injector.instance[JsonFileQuestionProvider]
  val ap = injector.instance[ApplicationProcessor]
  // This instance is intentionally created this way to show the difference between two different approaches (Guice and Object Constructor).
  val jfap = JsonFileApplicationProvider("applications.json")

  ap.acceptList(jfap.getApplications) match {
    case Success(acceptedApplication) => {
      val fw = new FileWriter("data/accepted.json")
      fw.write(writePretty[List[JobApplication]](acceptedApplication))
      fw.close()
      System.out.println("Processing completed, you can find all the accepted applications in file accepted.json under data folder.")
    }
    case Failure(e: ApplicationProcessingError) => {
      System.out.println(s"Processing failed: ${e.errorMessage}")
    }
    case Failure(e) => {
      System.out.println(s"Processing failed: ${e.getMessage}")
    }
  }
}