package main.scala

import com.google.inject.AbstractModule
import com.google.inject.name.Names
import net.codingwell.scalaguice.ScalaModule

/**
  * Created by Sheng on 11/19/16.
  */
class ApplicationProcessorModule extends AbstractModule with ScalaModule{
  def configure(): Unit = {
    bind[ApplicationProcessor].to[ApplicationProcessorImpl]
    bind[QuestionProvider].to[JsonFileQuestionProvider]
    // One example of instance binding
    bind[String].annotatedWith(Names.named("Question File Path"))
      .toInstance("questions.json")
  }
}
